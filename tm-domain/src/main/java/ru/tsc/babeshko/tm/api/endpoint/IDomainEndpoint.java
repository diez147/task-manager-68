package ru.tsc.babeshko.tm.api.endpoint;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.tsc.babeshko.tm.dto.request.*;
import ru.tsc.babeshko.tm.dto.response.*;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IDomainEndpoint extends IEndpoint {

    @NotNull
    String NAME = "DomainEndpoint";

    @NotNull
    String PART = NAME + "Service";

    @SneakyThrows
    @WebMethod(exclude = true)
    static IDomainEndpoint newInstance(@NotNull final String host, @NotNull final String port) {
        return IEndpoint.newInstance(host, port, NAME, NAMESPACE, PART, IDomainEndpoint.class);
    }

    @NotNull
    @WebMethod
    DataBackupLoadResponse loadDataBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBackupLoadRequest request
    );

    @NotNull
    @WebMethod
    DataBackupSaveResponse saveDataBackup(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBackupSaveRequest request
    );

    @NotNull
    @WebMethod
    DataBase64LoadResponse loadDataBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBase64LoadRequest request
    );

    @NotNull
    @WebMethod
    DataBase64SaveResponse saveDataBase64(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBase64SaveRequest request
    );

    @NotNull
    @WebMethod
    DataBinaryLoadResponse loadDataBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBinaryLoadRequest request
    );

    @NotNull
    @WebMethod
    DataBinarySaveResponse saveDataBinary(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataBinarySaveRequest request
    );

    @NotNull
    @WebMethod
    DataJsonLoadFasterXmlResponse loadDataJsonFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataJsonLoadFasterXmlRequest request
    );

    @NotNull
    @WebMethod
    DataJsonSaveFasterXmlResponse saveDataJsonFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataJsonSaveFasterXmlRequest request
    );

    @NotNull
    @WebMethod
    DataJsonLoadJaxbResponse loadDataJsonJaxb(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataJsonLoadJaxbRequest request
    );

    @NotNull
    @WebMethod
    DataJsonSaveJaxbResponse saveDataJsonJaxb(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataJsonSaveJaxbRequest request
    );

    @NotNull
    @WebMethod
    DataXmlLoadFasterXmlResponse loadDataXmlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataXmlLoadFasterXmlRequest request
    );

    @NotNull
    @WebMethod
    DataXmlSaveFasterXmlResponse saveDataXmlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataXmlSaveFasterXmlRequest request
    );

    @NotNull
    @WebMethod
    DataXmlLoadJaxbResponse loadDataXmlJaxb(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataXmlLoadJaxbRequest request
    );

    @NotNull
    @WebMethod
    DataXmlSaveJaxbResponse saveDataXmlJaxb(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataXmlSaveJaxbRequest request
    );

    @NotNull
    @WebMethod
    DataYamlLoadFasterXmlResponse loadDataYamlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataYamlLoadFasterXmlRequest request
    );

    @NotNull
    @WebMethod
    DataYamlSaveFasterXmlResponse saveDataYamlFasterXml(
            @WebParam(name = REQUEST, partName = REQUEST)
            @NotNull DataYamlSaveFasterXmlRequest request
    );

}
