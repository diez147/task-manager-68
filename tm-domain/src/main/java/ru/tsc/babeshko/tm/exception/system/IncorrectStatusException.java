package ru.tsc.babeshko.tm.exception.system;

import ru.tsc.babeshko.tm.exception.AbstractException;

public final class IncorrectStatusException extends AbstractException {

    public IncorrectStatusException(final String value) {
        super("Error! Incorrect status. Value `" + value + "` not found...");
    }

}