package ru.tsc.babeshko.tm.api.service.dto;

import org.jetbrains.annotations.Nullable;
import ru.tsc.babeshko.tm.dto.model.ProjectDto;

public interface IProjectDtoService extends IUserOwnedDtoService<ProjectDto> {

    void removeAllByUserId(@Nullable String userId);

}