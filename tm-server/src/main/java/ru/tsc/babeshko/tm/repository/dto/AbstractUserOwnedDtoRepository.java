package ru.tsc.babeshko.tm.repository.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.data.domain.Sort;
import org.springframework.data.repository.NoRepositoryBean;
import org.springframework.transaction.annotation.Transactional;
import ru.tsc.babeshko.tm.dto.model.AbstractUserOwnedModelDto;

import java.util.List;

@NoRepositoryBean
public interface AbstractUserOwnedDtoRepository<M extends AbstractUserOwnedModelDto> extends AbstractDtoRepository<M> {

    @NotNull
    List<M> findAllByUserId(@NotNull String userId);

    @NotNull
    List<M> findAllByUserId(@NotNull String userId, @NotNull Sort sort);

    @Nullable
    M findFirstByUserIdAndId(@NotNull String userId, @NotNull String id);

    @Transactional
    void deleteByUserIdAndId(@NotNull String userId, @NotNull String id);

    @Transactional
    void deleteAllByUserId(@NotNull String userId);

    long countByUserId(@NotNull String userId);

    boolean existsByUserIdAndId(@NotNull String userId, @NotNull String id);

}